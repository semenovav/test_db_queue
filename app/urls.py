from django.urls import include, path
from .views import demo_task

urlpatterns = [
    path("", demo_task),
]