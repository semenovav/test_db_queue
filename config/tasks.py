import time,logging

logger = logging.getLogger(__name__)


def my_task(job):
    print("Working hard...")
    time.sleep(10)
    print("Job's done!")